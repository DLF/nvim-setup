Just a repo where I'm trying to build up my next NeoVim config.

# To Do

## 🛠 Open
* [x] Get Rust-debugging running
* [x] Get Python LSP running
* [x] Get Python DAP running
* [x] Add markdown-preview plugin
* [x] Add Git plugins
* [ ] Chose and install a terminal plugin
* [x] Get yank-range flashing
* [x] Get this fancy indentation line thing.
      Or maybe something more grounded like
      `https://github.com/lukas-reineke/indent-blankline.nvim` 
* [x] Get Autocompletion to work.
* [x] Check for LSPs for natural langs and document langs (markdown, rst)
* [x] Check again if there are no better options for tab-line and status-line 
* [ ] Pimp `ltex` LSP (Keychord to toggle and change language)
* [x] Consider change to neo-tree

## 🍰 Check out other plugins
* https://github.com/phaazon/hop.nvim
* List at https://hannadrehman.com/top-neovim-plugins-for-developers-in-2022

# To get this running
* Have a virtual env at `~/.virtualenvs/debugpy/` with only `debugpy` installed.
* Have `sudo pacman -S shellcheck` installed for the Bash LSP
* Set `sudo archlinux-java set java-20-openjdk`. Otherwise, `ltex-ls` fails.

# Notes
* Maybe try [pylyzer](https://github.com/mtshiba/pylyzer) at some time as **Python LSP** and
  as a replacement for “Jedi”. It's supported by Mason and “lspconfig”.
  I just did not use it in the first place because it crashed with some stack overflow.
