return {
  'nvim-telescope/telescope.nvim',
  branch = '0.1.x',
  dependencies = {
    'nvim-lua/plenary.nvim',
    'stevearc/aerial.nvim',
    'nvim-telescope/telescope-ui-select.nvim',
  },
  config = function()
    require('telescope').setup({
      defaults = {
--        layout_strategy = 'center',
        layout_config = {
          height = 0.95,
          width = 0.95,
        },
      },
      extensions = {
        ["ui-select"] = {}
      },
    });
    require('telescope').load_extension('aerial');
    require("telescope").load_extension("ui-select");
  end,
}
