return {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function () 
      local configs = require("nvim-treesitter.configs")
      configs.setup({
          ensure_installed = {
            'c', 'cpp', 'go', 'lua', 'python', 'rust',
            'typescript', 'java', 'javascript',
            'vimdoc', 'vim', 'passwd',
            'yaml', 'toml', 'kdl', 'mermaid', 'markdown_inline', 'php',
            'latex',
            'bash', 'fish',
          },
          sync_install = false,
          highlight = { enable = true },
          indent = { enable = true },
        })
    end
 }

