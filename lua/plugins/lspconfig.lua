local f = io.popen ("/usr/bin/hostname")
local hostname = ""
if f ~= nil then
  hostname = f:read("*a") or ""
  f:close()
  hostname =string.gsub(hostname, "\n$", "")
end

return {
  'neovim/nvim-lspconfig',
  dependencies = {
    "williamboman/mason-lspconfig.nvim",
  },
  config = function()
    -- `rust-analyzer` is _not_ set up here.
    -- They are “ensured” by `mason`, but the setup is left
    -- to the `rust-tools` plugin.
    require("lspconfig").lua_ls.setup {};
    require("lspconfig").pyright.setup{};
    require'lspconfig'.ruff_lsp.setup{};
    require'lspconfig'.clangd.setup{};
    require('lspconfig').yamlls.setup {
      -- ... -- other configuration for setup {}
      -- settings = {
      --   yaml = {
      --     ... -- other settings. note this overrides the lspconfig defaults.
      --     schemas = {
      --       ["https://json.schemastore.org/github-workflow.json"] = "/.github/workflows/*",
      --       ["../path/relative/to/file.yml"] = "/.github/workflows/*",
      --       ["/path/from/root/of/project"] = "/.github/workflows/*",
      --     },
      --   },
      -- }
    };
    require'lspconfig'.bashls.setup{};
    require'lspconfig'.taplo.setup{};
    require'lspconfig'.esbonio.setup{};
    require'lspconfig'.marksman.setup{};
    require'lspconfig'.ltex.setup{
      autostart = false;
    };
    -- PHP LSP for the "arch" VM
    if hostname == "arch" then
      require'lspconfig'.phan.setup{}
      require'lspconfig'.intelephense.setup{}
    end
  end,
}
