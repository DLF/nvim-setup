return {
  "lukas-reineke/indent-blankline.nvim",
  config = function()
    local highlight = {
        "highlight_a",
        "highlight_b",
    }

    local hooks = require "ibl.hooks"
    -- create the highlight groups in the highlight setup hook, so they are reset
    -- every time the colorscheme changes
    hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
        vim.api.nvim_set_hl(0, "highlight_a", { fg = "#3c3836" })
        vim.api.nvim_set_hl(0, "highlight_b", { fg = "#504945" })
        vim.api.nvim_set_hl(0, "highlight_c", { fg = "#d3869b" })
    end)

    require("ibl").setup {
      indent = {
        highlight = highlight,
        char = "│",
      },
      scope = {
        enabled = true,
        highlight = {"highlight_c"},
        char = "│",
        show_start = false,
        show_end = false,
      }
    }
  end
}
