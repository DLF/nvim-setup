-- Update this path
local extension_path = vim.env.HOME .. '/.vscode-oss/extensions/vadimcn.vscode-lldb-1.9.2-universal/'
local codelldb_path = extension_path .. 'adapter/codelldb'
local liblldb_path = extension_path .. 'lldb/lib/liblldb.so'

return {
  'simrat39/rust-tools.nvim',
  dependencies = {
    "mfussenegger/nvim-dap",
    "neovim/nvim-lspconfig",
    "nvim-lua/plenary.nvim"
  },
  config = function()
    require("rust-tools").setup({
      dap = {
        adapter = require('rust-tools.dap').get_codelldb_adapter(
            codelldb_path, liblldb_path
        )
      },
      tools = { -- rust-tools options
        executor = require("rust-tools/executors").termopen,
        runnables = {
            use_telescope = true
        },
      },
    });
  end,
}
