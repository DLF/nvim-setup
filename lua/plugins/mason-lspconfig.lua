return {
  "williamboman/mason-lspconfig.nvim",
  dependencies = {
    "williamboman/mason.nvim",
  },
  config = function()
    require("mason-lspconfig").setup({
      ensure_installed = {
        "lua_ls",  -- Lua LSP
        "rust_analyzer",  -- Rust LSP
        "pyright",  -- Python LSP
        "ruff_lsp",  -- Python Linter
        "clangd",
        "yamlls",  -- yaml
        "bashls",  -- bash
        "taplo",  -- toml
        "esbonio",  -- Sphinx documentation LSP
        "marksman",  -- Markdown LSP, manages links (go to, references, completion..)
        "ltex",  -- natural language
      },
      -- automatic_installation = true,
    })
  end,
}
