return {
  "j-hui/fidget.nvim",
  requires = { "neovim/nvim-lspconfig" },
  tag = "legacy",
  config = function()
    require("fidget").setup()
  end,
}
