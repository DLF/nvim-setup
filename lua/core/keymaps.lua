local wk = require("which-key");

-- NORMAL mode basic bindings
wk.register(
  {
    -- Toggle views
    ["<c-n>"] =  { "<cmd>Neotree toggle<cr>", "Neo-Tree toggle"},
    ["<c-p>"] =  { "<cmd>AerialToggle<cr>", "Aerial toggle"},
    -- Split navigation
    ["<c-h>"] =  { "<c-w>h", "Split go left"},
    ["<c-j>"] =  { "<c-w>j", "Split go down"},
    ["<c-k>"] =  { "<c-w>k", "Split go up"},
    ["<c-l>"] =  { "<c-w>l", "Split go right"},
    -- Split resizing
    ["<c-a-h>"] =  { "<c-w>>", "Split horizontal increase"},
    ["<c-a-j>"] =  { "<c-w>+", "Split vertical increase"},
    ["<c-a-k>"] =  { "<c-w>-", "Split vertical decrease"},
    ["<c-a-l>"] =  { "<c-w><", "Split horizontal decrease"},
    -- Scoll Buffer
    ["<a-j>"] =  { "<c-e>", "Scroll down"},
    ["<a-k>"] =  { "<c-y>", "Scroll up"},
    -- Tab change
    ["<a-h>"] =  { "<cmd>tabprevious<cr>", "Tab change to previous"},
    ["<a-l>"] =  { "<cmd>tabnext<cr>", "Tab change to next"},
    -- Aerial jump
    ["{"] = { "<cmd>AerialPrev<cr>", "Jump to previous Aerial symbol" },
    ["}"] = { "<cmd>AerialNext<cr>", "Jump to next Aerial symbol" },
  }
)

-- VISUAL mode basic bindings
wk.register(
  {
    -- Intent while keeping selection
    ["<"] = { "<gv", "Indent left" },
    [">"] = { ">gv", "Indent right" },
  },
  {
    mode = "v"
  }
)

wk.register(
  {
    ["<leader>"] = {
      j = { require("joshuto").joshuto, "Open Joshuto"},
      m = { "<cmd>MarkdownPreviewToggle<cr>", "Markdown Preview Toggle"},
      x = {
        name = "Clear",
        s = {"<cmd>nohlsearch<cr>", "Clear Search"},
      },
      c = {
        name = "Colorizer",
        a = {'<cmd>ColorizerAttachToBuffer<cr>', "Attch colorizer to buffer"},
        d = {'<cmd>ColorizerDetachFromBuffer<cr>', "Deteach colorizer from buffer"},
        r = {'<cmd>ColorizerReloadAllBuffers<cr>', "Reload colorizer for all buffers"},
        t = {'<cmd>ColorizerToggle<cr>', "Toggle colorizer"},
      },
      s = {
        name = "Search",
        f = {function() require('telescope.builtin').find_files() end, "Search Files"},
        h = {function() require('telescope.builtin').help_tags() end, "Search Help"},
        w = {function() require('telescope.builtin').grep_string() end, "Search Word"},
        g = {function() require('telescope.builtin').live_grep() end, "Search by Grep"},
        d = {function() require('telescope.builtin').diagnostics() end, "Search Diagnostics"},
        s = {function() require('telescope').extensions.aerial.aerial() end, "Search Symbol"},
        r = {function() require('telescope.builtin').oldfiles() end, "Search Recent Files"},
        ["/"] = {
          function()
            require('telescope.builtin')
            .current_buffer_fuzzy_find(
              require('telescope.themes').get_dropdown {
                -- winblend = 10,
                --previewer = false,
              })
          end, "Search cur. Buffer fuzzy"
        },
      },
      l = {
        name = "LSP",
        q = {
            vim.lsp.buf.code_action({
              filter = function(a) return a.isPreferred end,
              apply = true
            }),
            "Quick Fix"
        },
        R = {vim.lsp.buf.rename, "Rename Symbol"},
        a = {vim.lsp.buf.code_action, "Code Action"},
        f = {vim.diagnostic.open_float, "Show information as float" },
        F = {vim.lsp.buf.format, "Format current buffer"},
        h = {vim.lsp.buf.hover, "Hover Documentation"},
        s = {vim.lsp.buf.signature_help, "Signature Documentation"},
        D = {vim.lsp.buf.type_definition, "Show type definition"},
        d = {vim.lsp.buf.definition, "Go to definition"},
        r = {require('telescope.builtin').lsp_references, "Go to references"},
        c = {vim.lsp.buf.declaration, "Go to declaration"},
        i = {vim.lsp.buf.implementation, "Go to implementation"},
        y = {require('telescope.builtin').lsp_document_symbols, "Search Document Symbols"},
        w = {
          name = "LSP Workspace stuff",
          s = {require('telescope.builtin').lsp_dynamic_workspace_symbols, "Search Workspace Symbols"},
          a = {vim.lsp.buf.add_workspace_folder, "Add workspace folder"},
          r = {vim.lsp.buf.remove_workspace_folder, "Remove workspace folder"},
          l = {
            function()
              print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
            end,
            "List workspace folder"
          },
        },
      },
      t = {
        name = "Trouble",
        t = {function() require("trouble").open() end, "Toggle"}
      },
      d = {
        name = "DAP",
        c = {require('dap').continue, "Continue"},
        t = {require('dap').terminate, "Terminate"},
        r = {require('dap').run_last, "Run Last"},
        b = {require('dap').toggle_breakpoint, "Toggle Breakpoint"},
        u = {require('dapui').toggle, "Toggle DAP UI"},
      },
      r = {
        name = "Rust Tools",
        d = {require('rust-tools').debuggables.debuggables, "Debuggables"},
        r = {require('rust-tools').runnables.runnables, "Runnables"},
        i = {require('rust-tools').inlay_hints.enable, "Enable Inlay hints"},
        I = {require('rust-tools').inlay_hints.disable, "Disable Inlay hints"},
        h = {
          function()
            require('rust-tools').hover_actions.hover_actions(); -- twice on purpose
            require('rust-tools').hover_actions.hover_actions(); -- see rust-tools readme
          end,
          "Hover menu"
        },
      },
      p = {
        name = "Python DAP",
        m = {require('dap-python').test_method, "Test Method" },
        c = {require('dap-python').test_class, "Test Class" },
        s = {require('dap-python').debug_selection, "Debug Selection" },
      },
      b = {
        name ="Blanks and Indentation",
        b = {"<cmd>IndentBlanklineToggle<cr>", "Toggle IndentBlankline"},
        s = {
          function()
            vim.o.list = not vim.o.list
          end,
          "Toggle white space visualization"
        },
      },
      [" "] = {function() require('telescope.builtin').buffers() end, "Search Buffers"},
      ["<"] = {function() vim.diagnostic.goto_prev() end, "Goto previous diagnostic finding" },
      [">"] = {function() vim.diagnostic.goto_next() end, "Goto previous diagnostic finding" },
    },
    ["<F7>"] = { require('dap').step_over, "DAP Step Over"},
    ["<F8>"] = { require('dap').step_into, "DAP Step Into"},
    ["<F9>"] = { require('dap').step_out, "DAP Step Out"},
  }
)

