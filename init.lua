-- Most basic configuration of NeoVim
require("core.base")

-- Bootsrap the Lazy plugin manager
require("core.lazy")

-- Load the plugins
require("lazy").setup("plugins")

-- Load the keymaps
require("core.keymaps")
